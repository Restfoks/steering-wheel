#pragma once
#include "stm32f4xx_hal.h"

namespace config
{
    static constexpr bool WHEEL_DIRECTION = true;
    
    static const auto WHEEL_ENCODER_A_PORT = GPIOA;
    static const auto WHEEL_ENCODER_A_PIN = GPIO_PIN_0;
    
    static const auto WHEEL_ENCODER_B_PORT = GPIOA;
    static const auto WHEEL_ENCODER_B_PIN = GPIO_PIN_1;
    
    static const auto     ACC_PEDAL_ADC_NUMBER = 1;
    static constexpr auto ACC_PEDAL_ADC_CHANNEL = ADC_CHANNEL_2;
    static const auto     ACC_PEDAL_ADC_PORT = GPIOA;
    static const auto     ACC_PEDAL_ADC_PIN = GPIO_PIN_2;
    static constexpr auto ACC_PEDAL_DIRECTION = true;
    
    static const auto     BRAKE_PEDAL_ADC_NUMBER = 1;
    static constexpr auto BRAKE_PEDAL_ADC_CHANNEL = ADC_CHANNEL_3;
    static const auto     BRAKE_PEDAL_ADC_PORT = GPIOA;
    static const auto     BRAKE_PEDAL_ADC_PIN = GPIO_PIN_3;
    static constexpr auto BRAKE_PEDAL_DIRECTION = true;
    
    static const auto     CLUTCH_PEDAL_ADC_NUMBER = 1;
    static constexpr auto CLUTCH_PEDAL_ADC_CHANNEL = ADC_CHANNEL_4;
    static const auto     CLUTCH_PEDAL_ADC_PORT = GPIOA;
    static const auto     CLUTCH_PEDAL_ADC_PIN = GPIO_PIN_4;
    static constexpr auto CLUTCH_PEDAL_DIRECTION = true;
    
    static constexpr uint8_t SHIFTER_BUTTON_ACTIVE_LEVEL = 1;
}