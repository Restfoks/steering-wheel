#pragma once
#include "stm32f4xx_hal.h"

class AnalogIn
{
public:
    AnalogIn(uint8_t adc_number, uint32_t adc_channel
        , GPIO_TypeDef* port, uint32_t pin);
    AnalogIn(AnalogIn&) = delete;
    AnalogIn operator = (AnalogIn&) = delete;
    uint16_t read_u16();
private:
    ADC_HandleTypeDef* _hadc = nullptr;
    ADC_ChannelConfTypeDef _sConfig;
};