#include "analog_in.h"



AnalogIn::AnalogIn(uint8_t adc_number, 
    uint32_t adc_channel,
    GPIO_TypeDef* port,
    uint32_t pin)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(port, &GPIO_InitStruct);
    
    static ADC_HandleTypeDef handle_adc[3];

    switch (adc_number)
    {
    case 1:
        _hadc = &handle_adc[0];
        handle_adc[0].Instance = ADC1;
        __HAL_RCC_ADC1_CLK_ENABLE();
        break;
    case 2:
        _hadc = &handle_adc[1];
        handle_adc[1].Instance = ADC2;
        __HAL_RCC_ADC2_CLK_ENABLE();
        break;
    case 3:
        _hadc = &handle_adc[2];
        handle_adc[2].Instance = ADC3;
        __HAL_RCC_ADC3_CLK_ENABLE();
        break;
    default:
        while (1) ;
    }
    ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
    //_hadc->Instance = ADC1;
    
    _hadc->Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    _hadc->Init.Resolution = ADC_RESOLUTION_12B;
    _hadc->Init.ScanConvMode = DISABLE;
    _hadc->Init.ContinuousConvMode = DISABLE;
    _hadc->Init.DiscontinuousConvMode = DISABLE;
    _hadc->Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    _hadc->Init.ExternalTrigConv = ADC_SOFTWARE_START;
    _hadc->Init.DataAlign = ADC_DATAALIGN_LEFT;
    _hadc->Init.NbrOfConversion = 1;
    _hadc->Init.DMAContinuousRequests = DISABLE;
    _hadc->Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    if (HAL_ADC_Init(_hadc) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    _sConfig.Channel = adc_channel;
    _sConfig.Rank = 1;
    _sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;

}


uint16_t AnalogIn::read_u16()
{
    while (HAL_ADC_ConfigChannel(_hadc, &_sConfig) != HAL_OK)
    {
    }
    HAL_ADC_Start(_hadc);
    if (HAL_ADC_PollForConversion(_hadc, 10'000) == HAL_OK)
    {
        uint16_t value = HAL_ADC_GetValue(_hadc);
        HAL_ADC_Stop(_hadc);
        return value;
    };
    HAL_ADC_Stop(_hadc);
    return 0;
}
