#include "digital_in_out.h"

DigitalInOut::DigitalInOut(GPIO_TypeDef* port, uint32_t pin)
{
    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    
    _port = port;
    /*Configure GPIO pins : PA0 PA1 */
    _GPIO_InitStruct.Pin = pin;
    _GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    _GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(port, &_GPIO_InitStruct);
}

void DigitalInOut::output()
{
    _GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(_port, &_GPIO_InitStruct);
}


void DigitalInOut::input()
{
    _GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    HAL_GPIO_Init(_port, &_GPIO_InitStruct);
}


void DigitalInOut::pull_up()
{
    _GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(_port, &_GPIO_InitStruct);
}


void DigitalInOut::pull_down()
{
    _GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(_port, &_GPIO_InitStruct);
}


void DigitalInOut::pull_none()
{
    _GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(_port, &_GPIO_InitStruct);
}

uint8_t DigitalInOut::read()
{
    return HAL_GPIO_ReadPin(_port, _GPIO_InitStruct.Pin);
}

void DigitalInOut::write(int state)
{
    HAL_GPIO_WritePin(_port
        , _GPIO_InitStruct.Pin
        , static_cast < GPIO_PinState>(state));
}
