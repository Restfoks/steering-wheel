#pragma once
#include "stm32f4xx_hal.h"

class DigitalInOut
{
public:
    DigitalInOut(GPIO_TypeDef* port, uint32_t pin);
    void output();
    void input();
    void pull_up();
    void pull_down();
    void pull_none();
    uint8_t read();
    void write(int);
private:
    GPIO_InitTypeDef _GPIO_InitStruct;
    GPIO_TypeDef* _port;
};