#pragma once

#include "analog_in/analog_in.h"
#include "digital_in_out/digital_in_out.h"

class NonCopyable
{
public:
    NonCopyable() = default;
private:
    NonCopyable(NonCopyable&) = delete;
    NonCopyable operator =(NonCopyable&) = delete;
};