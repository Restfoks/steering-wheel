#include "wheel_sensors.h"
static AnalogIn acceleration_pedal_analog_in(config::ACC_PEDAL_ADC_NUMBER,
    config::ACC_PEDAL_ADC_CHANNEL,
    config::ACC_PEDAL_ADC_PORT,
    config::ACC_PEDAL_ADC_PIN);

static AnalogIn brake_pedal_analog_in(config::BRAKE_PEDAL_ADC_NUMBER,
    config::BRAKE_PEDAL_ADC_CHANNEL,
    config::BRAKE_PEDAL_ADC_PORT,
    config::BRAKE_PEDAL_ADC_PIN);

static AnalogIn clutch_pedal_analog_in(config::CLUTCH_PEDAL_ADC_NUMBER,
    config::CLUTCH_PEDAL_ADC_CHANNEL,
    config::CLUTCH_PEDAL_ADC_PORT,
    config::CLUTCH_PEDAL_ADC_PIN);

WheelSensors::WheelSensors()
    : _acceleration(acceleration_pedal_analog_in, config::ACC_PEDAL_DIRECTION)
    , _brake(brake_pedal_analog_in, config::BRAKE_PEDAL_DIRECTION)
    , _clutch(clutch_pedal_analog_in, config::CLUTCH_PEDAL_DIRECTION)  
{
}

void WheelSensors::update()
{
    _wheel.update();
    _acceleration.update();
    _brake.update();
    _clutch.update();
}

void WheelSensors::reset()
{
    _wheel.reset();
    _acceleration.reset();
    _brake.reset();
    _clutch.reset();
}

WheelData& WheelSensors::get_data()
{
    _data.wheel_rotation = _wheel.get_value();
    _data.acceleration = _acceleration.get_value();
    _data.brake = _brake.get_value();
    _data.clutch = _clutch.get_value();
    return _data;
}

