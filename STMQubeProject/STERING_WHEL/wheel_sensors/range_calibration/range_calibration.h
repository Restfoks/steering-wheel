#pragma once
#include "stm32f4xx_hal.h"
#include <cassert>
#include <wheel_sensors/utils/line_equation.h>

template <class InType, class OutType>
class RangeCalibration
{
public:
    RangeCalibration(OutType output_value_minimum,
        OutType output_value_maximum,
        InType input_value_now)
        : _out_min(output_value_minimum)
        , _out_max(output_value_maximum)
    {
        reset_calibration(input_value_now, input_value_now);
    }
    OutType calibrate(InType raw_value)
    {
        if (raw_value < _min)
            _min = raw_value;
        if (raw_value > _max)
            _max = raw_value;
        const utils::LineEquation<InType, OutType> in_to_out(_min, _out_min, _max, _out_max);
        return in_to_out.get_y(raw_value);
    }
    void reset_calibration(InType in_value_minimum, InType in_value_maximum)
    {
        _min = in_value_minimum;
        _max = in_value_maximum;        
    }
    
private:
    InType _min, _max;
    const OutType _out_min, _out_max;
};

