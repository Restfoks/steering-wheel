#pragma once
#include <cassert>
#include <stm32f4xx_hal.h>

#include <config.h>
#include <drivers_foks.h>
#include <wheel_sensors/range_calibration/range_calibration.h>

class Pedal
{
    static constexpr uint8_t MAX_VALUE = 255;
    static constexpr uint8_t MIN_VALUE = 0;
public:
    Pedal(AnalogIn& adc, bool direction);
    void update();
    void reset();
    int8_t get_value();
private:
    AnalogIn& _adc;
    int32_t _current_value = 0;
    RangeCalibration<int32_t,int16_t> _calibration;
};
