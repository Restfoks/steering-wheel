#include "pedal.h"

Pedal::Pedal(AnalogIn& adc, bool direction)
    : _current_value(_adc.read_u16())
    , _adc(adc)
    , _calibration(direction ? MAX_VALUE : MIN_VALUE
                    , direction ? MIN_VALUE : MAX_VALUE
                    , _current_value)
{
}

void Pedal::update()
{
}

void Pedal::reset()
{
    _current_value = _adc.read_u16();
    _calibration.reset_calibration(_current_value, _current_value);
}

int8_t Pedal::get_value()
{
    _current_value = _adc.read_u16();
    return _calibration.calibrate(_current_value);
}