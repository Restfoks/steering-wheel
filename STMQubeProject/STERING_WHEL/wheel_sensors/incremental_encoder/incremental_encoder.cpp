#include "incremental_encoder.h"

IncrementalEncoder::IncrementalEncoder(DigitalInOut& a, DigitalInOut& b)
    : _A(a), _B(b)
{
    _A.input();
    _B.input();
    _A.pull_none();
    _B.pull_none();
    HAL_Delay(5);
    const auto configuration = get_configuration();
    current_configuration = configuration;
}

int8_t IncrementalEncoder::get_shift_from_prev_state()
{
    const auto next_configuration = get_configuration();
    if (next_configuration == current_configuration)
        return 0;
    int8_t shift = 0;
    if (next_configuration == current_configuration.next())
    {
        shift = 1;
        ++current_configuration;
    }
    else if (next_configuration == current_configuration.prev())
    {
        shift = -1;
        --current_configuration;
    }
    else
    {
        current_configuration = next_configuration;
    }
    return shift;
}

uint8_t IncrementalEncoder::get_configuration()
{
    const bool a = _A.read();
    const bool b = _B.read();
    return b + (a << 1);
}

uint8_t IncrementalEncoder::EncoderIterator::operator =(uint8_t a)
{
    if (a > 0b11)
        index = 0;
    for (int i = 0; i < SIZE; i++)
    {
        if (a == configurations[i])
        {
            index = i;
            break;
        }
    }
    return configurations[index];
}

IncrementalEncoder::EncoderIterator::operator uint8_t()
{
    return configurations[index];
}

uint8_t IncrementalEncoder::EncoderIterator::next()
{
    const auto next_index = (index + 1) % SIZE;
    return configurations[next_index];
}
uint8_t IncrementalEncoder::EncoderIterator::prev()
{
    const auto prev_index = (index + SIZE - 1) % SIZE;
    return configurations[prev_index];
}
uint8_t IncrementalEncoder::EncoderIterator::operator ++()
{
    index = (index + 1) % SIZE;
    return configurations[index];
}
uint8_t IncrementalEncoder::EncoderIterator::operator --()
{
    index = (index + SIZE - 1) % SIZE;
    return configurations[index];
}