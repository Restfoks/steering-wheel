#pragma once
#include "stm32f4xx_hal.h"
#include <drivers_foks.h>
#include <config.h>
#include <cassert>

class IncrementalEncoder
{
public:
    IncrementalEncoder(DigitalInOut& a, DigitalInOut& b);
    int8_t get_shift_from_prev_state();
    
private:
    uint8_t get_configuration();
    DigitalInOut& _A;
    DigitalInOut& _B;
    class EncoderIterator
    {
        static constexpr size_t SIZE = 4;
    public:
        uint8_t operator =(uint8_t a);
        operator uint8_t();
        uint8_t next();
        uint8_t prev();
        uint8_t operator ++();
        uint8_t operator --();
    private:
        uint8_t index = 0;
        const uint8_t configurations[SIZE] = { 0b11, 0b10, 0b00, 0b01 }; 
    } current_configuration;  
};
