#pragma once
#include "stm32f4xx_hal.h"
#include <drivers_foks.h>
#include <config.h>
#include <cassert>

#include <wheel_sensors/wheel/wheel.h>
#include <wheel_sensors/pedal/pedal.h>
struct WheelData
{
    int16_t wheel_rotation;
    uint8_t acceleration;
    uint8_t brake;
    uint8_t clutch;
    void operator =(const WheelData& in)
    {
        this->wheel_rotation = in.wheel_rotation;
        this->acceleration = in.acceleration;
        this->brake = in.brake;
        this->clutch = in.clutch;
    }
};
class WheelSensors
{
public:
    WheelSensors();
    void update();
    void reset();
    WheelData& get_data();
private:
    Wheel _wheel;
    Pedal _acceleration;
    Pedal _brake;
    Pedal _clutch;
    WheelData _data;
};