#pragma once



namespace utils

{

    template <class TypeX, class TypeY>

    class LineEquation

    {

    public:

        LineEquation(TypeX x1, TypeY y1, TypeX x2, TypeY y2)

            : _y1(y1)

            , _x1(x1)

            , _delta_x(x2 - x1)

            , _delta_y(y2 - y1)

        {}



        TypeX get_x(const TypeY& y) const

        {

            return (y - _y1) * _delta_x / _delta_y + _x1;

        }



        TypeY get_y(const TypeX& x) const

        {

            const TypeY ret = (x - _x1) * _delta_y / _delta_x + _y1;

            return ret;

        }

    private:

        const TypeY _delta_y, _y1;

        const TypeX _delta_x, _x1;

    };

}
