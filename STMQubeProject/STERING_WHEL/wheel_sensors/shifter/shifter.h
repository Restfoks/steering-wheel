#pragma once
#include <drivers_foks.h>
#include <config.h>


enum Gear
{
    Gear1 = 0,
    Gear2 = 1,
    Gear3 = 2,
    Gear4 = 3,
    Gear5 = 4,
    Gear6 = 5,
    GearReverse = 6,
    GearNeutral = 7
};
class Shifter
{
    static constexpr uint8_t BUTTONS_COUNT = 7;
    static constexpr uint8_t ACTIVE_LEVEL = config::SHIFTER_BUTTON_ACTIVE_LEVEL;
public:
    Shifter();
    
    void update();
    bool is_changed();
    Gear current_gear();

private:
    DigitalInOut* buttons[BUTTONS_COUNT];
    
    bool _changed = true;
    Gear _current_gear = GearNeutral;
};