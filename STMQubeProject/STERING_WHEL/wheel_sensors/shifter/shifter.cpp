#include "shifter.h"

//DigitalInOut gear_1;
//DigitalInOut gear_2;
//DigitalInOut gear_3;
//DigitalInOut gear_4;
//DigitalInOut gear_5;
//DigitalInOut gear_6;
//DigitalInOut gear_Reverse;

Shifter::Shifter()
    //: buttons{ &gear_1, &gear_2, &gear_3, &gear_4, &gear_5, &gear_6, &gear_Reverse }
{
    constexpr bool active_level_is_right = (ACTIVE_LEVEL == 0) || (ACTIVE_LEVEL == 1);
    static_assert(active_level_is_right, "Active level must be 1 or 0");
    for (auto but : buttons)
    {
        but->input();
        if (ACTIVE_LEVEL == 0)
            but->pull_up();
        else
            but->pull_down();
    }
    
}

void Shifter::update()
{
    for (uint8_t i = 0; i < BUTTONS_COUNT; i++)
    {
        if (buttons[i]->read() == ACTIVE_LEVEL)
        {
            Gear new_gear = static_cast<Gear>(i);
            if (new_gear != _current_gear)
                _changed = true;
            _current_gear = new_gear;
            return;
        }
    }
    
}
