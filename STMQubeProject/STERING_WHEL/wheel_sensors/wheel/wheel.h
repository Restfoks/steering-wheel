#pragma once
#include <cassert>
#include <stm32f4xx_hal.h>

#include <config.h>
#include <drivers_foks.h>
#include <wheel_sensors/range_calibration/range_calibration.h>
#include <wheel_sensors/incremental_encoder/incremental_encoder.h>

class Wheel
{
public:
    Wheel();
    void update();
    void reset();
    int16_t get_value();
private:
    int32_t current_value = 0;
    IncrementalEncoder _encoder;
    RangeCalibration<int32_t,int32_t> _calibration;
    const bool direction = config::WHEEL_DIRECTION;
};
