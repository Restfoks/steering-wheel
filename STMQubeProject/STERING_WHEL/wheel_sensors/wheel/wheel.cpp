#include "wheel.h"

static DigitalInOut encoder_pin_a(config::WHEEL_ENCODER_A_PORT, config::WHEEL_ENCODER_A_PIN);
static DigitalInOut encoder_pin_b(config::WHEEL_ENCODER_B_PORT, config::WHEEL_ENCODER_B_PIN);

Wheel::Wheel()
    : _encoder(encoder_pin_a, encoder_pin_b)
    , _calibration(-32768, 32767, current_value)
{
    
}

void Wheel::update()
{
    current_value += _encoder.get_shift_from_prev_state();
}

void Wheel::reset()
{
    current_value = 0;
    _calibration.reset_calibration(current_value, current_value);
}

int16_t Wheel::get_value()
{
    return _calibration.calibrate(current_value);
}